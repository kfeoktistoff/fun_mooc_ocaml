(*

Simple uses of references (50/50 points)

    Define swap : 'a ref -> 'a ref -> unit that swaps the contents of two references.
    Define update : 'a ref -> ('a -> 'a) -> 'a that calls a function on the contents of a reference, updates it with the result, and returns the old value.
    For instance let r = ref 6 in update r (function x -> x + 1) should return 6 and set the reference to 7.
    Define move: 'a list ref -> 'a list ref -> unit, that removes the top argument from the first list and puts it on top of the second list. If the first list is empty, it should raise Empty.
    A common pattern is to use a reference to perform a computation in an imperative way, but to keep it in a local definition, completely invisible from outside the function implementation.
    Define reverse: 'a list -> 'a list, that has a type and an observable behaviour that look like the ones of a purely functional function, buf that use a reference internally to perform the computation. It takes a list, and returns a copy of the list whose elements are in reverse order.
    The only functions you can call, except from locally defined functions, are (!), (:=), ref, and move that you just defined. And you are not allowed to use pattern matching.

The given prelude

exception Empty ;;

*)

let swap ra rb =
  let tmp = !ra in
  ra := !rb;
  rb := tmp
;;

let update r f =
  let tmp = !r in
  r := f (!r);
  tmp
;;

let move l1 l2 =
  if List.length (!l1) = 0 then raise Empty else
    match !l2 with
    | [] -> l2 := [List.hd (!l1)]; l1 := List.tl (!l1)
    | _ -> l2 := List.rev (List.rev (!l2) @ [List.hd (!l1)]); l1 := List.tl (!l1)
;;

let reverse l =
  let refl = ref l and revl = ref [] in
  let rec reverse' l' =
    try
      move refl revl;
      reverse' refl
    with Empty -> revl
  in 
  !(reverse' refl) 
;;
