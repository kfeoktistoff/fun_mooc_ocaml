(*

Classic functions over lists (40/40 points)

In this exercise, we implement the classic functions over lists.

    Write a function mem : int -> int list -> bool such that mem x l is true if and only if x occurs in l.
    Write a function append : int list -> int list -> int list such that append l1 l2 is the concatenation of l1 and l2.
    Write a function combine : int list -> int list -> (int * int) list such that combine l1 l2 is the list of pairs obtained by joining the elements of l1 and l2. This function assumes that l1 and l2 have the same length. For instance, combine [1;2] [3;4] = [(1, 3); (2, 4)].
    Write a function assoc : (string * int) list -> string -> int option such that assoc l k = Some x if (k, x) is the first pair of l whose first component is k. If no such pair exists, assoc l k = None.

*)

let rec mem x l = match l with
  | [] -> false
  | h::t -> if h = x then true else mem x t;;

let rec append l1 l2 = 
  match l1 with
  | [] -> l2
  | h::t -> h::(append t l2);;

let rec combine l1 l2 =
  match l1, l2 with
  | [], [] -> []
  | h1::[], h2::[] -> [(h1,h2)]
  | h1::t1, h2::t2 -> [(h1,h2)] @ (combine t1 t2)
;;

let rec assoc (l:(string * int) list) (k:string) =
  match l with
  | [] -> None
  | h::t -> match h with
    | (k', x) -> if k' = k then (Some x) else assoc t k;;
