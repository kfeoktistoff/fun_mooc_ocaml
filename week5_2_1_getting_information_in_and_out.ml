(*

Printing lists (200/200 points)

    Write a function print_int_list : int list -> unit that takes a list of integers as input, and prints all the elements of the list, each on its own line.
    Write a function print_every_other : int -> int list -> unit that takes a value k and a list of integers as input, and prints the elements of the list that are in positions multiple of k, each on its own line. Note: the first element of a list is at the position 0, not 1.
    Write a function print_list : ('a -> unit) -> 'a list -> unit that takes a printer of values of some type 'a and a list of such values as input, and prints all the elements of the list, each on its own line.

*)

let rec print_int_list l = List.fold_left (fun () e -> print_string(string_of_int(e) ^ "\n")) () l;;

let print_every_other k l = List.iteri (fun i e -> if i mod k = 0 then print_string(string_of_int(e) ^ "\n") else ()) l;;

let rec print_list (print:('a -> unit)) (l:('a list)) = List.fold_left (fun () e -> (print e; print_newline())) () l;;
