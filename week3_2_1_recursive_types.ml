(*

First In First Out (50/50 points)

A queue is a standard FIFO data structure. See wikipedia

In this exercise, we implement a queue with a pair of two lists (front, back) such that front @ List.rev back represents the sequence of elements in the queue.

    Write a function is_empty : queue -> bool such that is_empty q is true if and only if q has no element.
    Write a function enqueue : int -> queue -> queue such that enqueue x q is the queue as q except that x is at the end of the queue.
    Write a function split : int list -> int list * int list such that split l = (front, back) where l = back @ List.rev front and the length of back and front is List.length l / 2 or List.length l / 2 + 1
    Write a function dequeue : queue -> int * queue such that dequeue q = (x, q') where x is the front element of the queue q and q' corresponds to remaining elements. This function assumes that q is non empty.

The given prelude

type queue = int list * int list

*)

let is_empty = function
  | (([],[]):queue) -> true
  | _ -> false;;

let enqueue (x:int) q = match q with
  | ((front, back):queue) -> (front, [x] @ back);;
    
let split l = match l with
  | [] -> ([],[])
  | front :: back -> 
      let rec split' h t = if (List.length h) < ((List.length h) + (List.length t))/2
        then split' (h @ [List.hd t]) (List.tl t)
        else (List.rev t, h)
      in split' [] l;;

let rec dequeue q = match q with
  | (front, []) -> ((List.hd front), ((List.tl front), []))
  | ([], back) -> dequeue (split back)
  | (front, back) -> ((List.hd front), ((List.tl front), back))
;;
