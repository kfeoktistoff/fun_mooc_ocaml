(*

Balanced binary trees (22/22 points)

A binary tree t, of the 'a bt type given in the prelude, is either an empty tree, or the root of a tree with a value and two children subtrees.

    Write a function height : 'a bt -> int that computes the height of a tree.
    A tree is balanced if, for all internal node n, its two subtrees have the same height. Write a function balanced : 'a bt -> bool that tells if a tree is balanced.

The given prelude

type 'a bt =
  | Empty
  | Node of 'a bt * 'a * 'a bt ;;

*)

let rec height t = 
  let rec height' t n =
    match t with
    | Empty -> 0
    | Node(Empty, x, Empty) -> n+1
    | Node(lnode, x, Empty) -> height' lnode (n+1)
    | Node(Empty, x, rnode) -> height' rnode (n+1)
    | Node(lnode, x, rnode) -> if (height' rnode (n+1)) > (height' lnode (n+1)) then (height' rnode (n+1)) else (height' lnode (n+1))
  in height' t 0
;;

let rec balanced t  = 
  match t with
  | Empty -> true
  | Node(lnode, x, Empty) when (height lnode) > 0 -> false
  | Node(Empty, x, rnode) when (height rnode) > 0 -> false
  | Node(lnode, x, rnode) -> height(lnode) = height(rnode) && balanced(lnode) && balanced(rnode)
;;