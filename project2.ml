(*

Random Text Generation (413/413 points)
word 	→ 	next 	freq
"START" 	→ 	"I" 	100%
"I" 	→ 	"am" 	100%
"am" 	→ 	"a" 	100%
"a" 	→ 	"man" 	25%
	| 	"good" 	75%
"man" 	→ 	"and" 	50%
	| 	"STOP" 	50%
"and" 	→ 	"my" 	50%
	| 	"a" 	50%
"my" 	→ 	"dog" 	100%
"dog" 	→ 	"is" 	33%
	| 	"and" 	33%
	| 	"makes" 	34%
"good" 	→ 	"dog" 	66%
	| 	"man" 	34%
"is" 	→ 	"a" 	100%
"makes" 	→ 	"a" 	100%

The goal of this project is to synthesize natural language sentences using information extracted from an existing text corpus.

For this, given a text corpus as input, we will first compute the frequency of all sequences of two words in the original text; then we will use this information to produce new sentences by randomly collating these sequences with the same frequencies.

This method is known under the term of Markov chain. From the input text, we compute a transition table that associates to each word the list of words that may appear after it, with their relative frequencies.

For instance, if we examine ”I am a man and my dog is a good dog and a good dog makes a good man”, delimiting it with ”START” and ”STOP” to identify the beginning and end of the sentence, we end up with the transition table on the right.

This table can then be used to generate new text that ressembles the input in the following way: starting from the "START" word, choose one of the words that may appear after it, with the probability found in the table, add it to the output, then iterate the process until the "STOP" word is found. Below are some example sentences produced using this table.

START I am a good  man STOP ; START I am a good dog  is a good dog and
my dog and my  dog is a man and my  dog and a man STOP ;  START I am a
good dog is a man and my dog makes a good man STOP ; START I am a good
dog makes a good dog is a good dog  and a good dog makes a good dog is
a man STOP ; START I am a good dog and a man and a good dog and a good
man and a good dog is a good dog  is a good man and a man STOP ; START
I am a good dog and a good dog and my dog is a man STOP ; START I am a
man STOP ;  START I am a good dog  is a good dog is a  good dog and my
dog is a man STOP ; START I am a good man STOP ; START I am a good dog
makes a good dog and a good dog is a good dog is a good man and my dog
is a good dog and  my dog and a good man and a good  dog is a good man
STOP ; START I am a man and my dog and my dog is a good dog and a good
dog makes a man STOP

This project is decomposed in three parts.

    First, we will build a quick prototype, that goes from an input sentence to a randomly generated sentences via a distribution table as the ones above.
    Then we will use better data structures to enhance the performance so that we can use larger texts, such as small books, as input.
    After that, we will enhance the quality of input and output, by analysing in a smarter way the input text corpus, and by considering sequences of more than two words.

Note: this project may take more time to be graded, because it is longer than simple exercises, and because it is tested on large inputs. We suggest that you use the typecheck button and the toplevel extensively, so that you are reasonnably sure of your code before submitting it to the tests. Also, we added a function grade_only : int list -> unit, that you may call in your code to select the exercises to grade. All other exercises won't be graded at all, and considered failed. For instance, if you write grade_only [ 3 ] ;; at the beginning of your file, only exercise 3 will be tested.
Part A: A First Draft

Our first goal will be to build such a table and generate sentences from it, quick and dirty style, using lists and their predefined operators. Consider using as much as possible the List module (List.assoc, List.length, List.nth, etc.) and don't think about efficiency.

In this exercise, we will use associative lists as the data structure that links each word to its possible suffixes. Associative lists are often used in prototypes or non critical programs because they are very easy to use and debug. Their major downfall is the complexity of searching for an element.
The type of an associative list that maps string keys to 'a values is simply (string * 'a) list. The value associated with a key "x" is simply the right component of the first pair in the list whose left component is "x". This lookup is already defined in the standard library as List.assoc. Hence, setting the value of "x" to 3, for instance, is just adding ("x",3) in front of the list. To remove an element, you can just use List.filter with the right predicate.

The type of lookup tables for this exercise is

type ltable = (string * string list) list

    Write a function words : string -> string list that takes a sentence and returns the list of its words. As a first approximation, will work on single sentences in simple english, so you can consider sequences of roman letters and digits as words, and everything else as separators. If you want to build words bit by bit, you can experiment with the Buffer module. Beware, this preliminary function may not be as easy as it seems.
    Write build_ltable : string list -> ltable that builds an associative list mapping each word present in the input text to all its possible successors (including duplicates). The table should also contain "START" that points to the first word and "STOP" that is pointed by the last word.
    For instance, a correct (and minimal) table for "x y z y x y" looks like:

    [ ("z", [ "y" ]);
      ("x", [ "y" ; "y" ]);
      ("START", [ "x" ]);
      ("y", [ "x" ; "z" ; "STOP" ]) ]
        

    Write the random selection function next_in_ltable : (string * string list) list -> string - > string which takes a table, a given word and returns a valid successor of this word. Your function should respect the probability distribution (which should be trivially ensured by the presence of the duplicates in the successor lists).
    Write the random generation function walk_ltable : (string * string list) list -> string list which takes a table and returns a sequence of words that form a valid random sentence (without the "START" and "STOP").

You can use display_quote: string list -> unit to display the generated texts.
Part B: Performance Improvements

Now, we want to use more efficient data structures, so that we can take larger inputs and build bigger transition tables.

In this exercise, we will use hash tables, predefined in OCaml in the Hashtbl module. Used correctly, hash table provide both fast insertion and extraction. Have a look at the documentation of the module. In particular, don't miss the difference between Hashtbl.add and Hashtbl.replace (you'll probably want to use the latter most of the time).

The types for this exercise are:

type distribution =
  { total : int ;
    amounts : (string * int) list }
type htable = (string, distribution) Hashtbl.t

    In the simple version, we stored for each word the complete list of suffixes, including duplicates. This is a valid data structure to use when building the table since adding a new suffix in front of the list is fast. But when generating, it means computing the length of this list each time, and accessing its random nth element, which is slow if the list is long.
    Write compute_distribution : string list -> distribution that takes a list of strings and returns a pair containing its length and an association between each string present in the original list and its number of occurrences.
    For instance, compute_distribution ["a";"b";"c";"b";"c";"a";"b";"c";"c";"c"] should give { total = 10 ; amounts = [("c", 5); ("b", 3); ("a", 2)] }.
    Hint: a first step that simplifies the problem is to sort the list.
    Write a new version of build_htable : string list -> htable that creates a hash table instead of an associative list, so that both table building and sentence generation will be faster.
    Like the associative list, the table is indexed by the words, each word being associated to its successors. But instead of just storing the list of successors, it will use the format of the previous question.
    Hint: You can first define an intermediate table of type (string, string list) Hashtbl.t that stores the lists of successors with duplicates. Then you traverse this intermediate table with Hashtbl.iter, and for each word, you add the result of compute_distribution in the final table.
    Define next_in_htable : htable -> string -> string that does the same thing as next_in_ltable for the new table format.
    Finally, define walk_htable : htable -> string list

Part C: Quality Improvements

    If we want to generate sentences from larger corpuses, such as the ones of the ebooks_corpus given in the prelude, we cannot just ignore the punctuation. We also want to generate text using not only the beginning of the original text, but the start of any sentence in the text.
    Define sentences : string -> string list list that splits a string into a list of sentences such as:
        uninterrupted sequences of roman letters, numbers, and non ASCII characters (in the range '\128'..'\255') are words;
        single punctuation characters ';', ',', ':', '-', '"', '\'', '?', '!' and '.' are words;
        punctuation characters '?', '!' and '.' terminate sentences;
        everything else is a separator;
        and your function should not return any empty sentence. 

Now, we will drastically improve the results by matching sequences of more than two words. We will thus update the format of our tables again, and use the following ptable type (which looks a lot like the previous one).

type ptable =
  { prefix_length : int ;
    table : (string list, distribution) Hashtbl.t }

So let's say we want to identify sequences of N words in the text. The prefix_length field contains N−1. The table field associates each list of N−1 words from the text with the distribution of its possible successors.
prefix 	→ 	next 	freq
["START"; "START"] 	→ 	"I" 	100%
["START"; "I"] 	→ 	"am" 	100%
["I"; "am"] 	→ 	"a" 	100%
["am"; "a"] 	→ 	"man" 	100%
["man"; "and"] 	→ 	"my" 	100%
["is"; "a"] 	→ 	"good" 	100%
["and"; "my"] 	→ 	"dog" 	100%
["my"; "dog"] 	→ 	"is" 	100%
["makes"; "a"] 	→ 	"good" 	100%
["a"; "good"] 	→ 	"man" 	33%
	| 	"dog" 	66%
["dog"; "is"] 	→ 	"a" 	100%
["and"; "a"] 	→ 	"good" 	100%
["good"; "dog"] 	→ 	"makes" 	50%
	| 	"and" 	50%
["dog"; "and"] 	→ 	"a" 	100%
["a"; "man"] 	→ 	"and" 	100%
["good"; "man"] 	→ 	"STOP" 	100%
["dog"; "makes"] 	→ 	"a" 	100%

The table on the right gives the lookup table for the example given at the beginning of the project: ”I am a man and my dog is a good dog and a good dog makes a good man”, and a size of 2. You can see that the branching points are fewer and make a bit more sense.
As you can see, we will use "STOP" as an end marker as before. But instead of a single "START" we will use as a start marker a prefix of the same size as the others, filled with "START".

    First, define start: int -> string list that makes the start prefix for a given size (start 0 = [], start 1 = [ "START" ], start 2 = [ "START" ; "START" ], etc.).
    Define shift: string list -> string -> string list. It removes the front element of the list and puts the new element at the end. (shift [ "A" ; "B" ; "C" ] "D" = [ "B" ; "C" ; "D" ], shift [ "B" ; "C" ; "D" ] "E" = [ "C" ; "D" ; "E" ], etc.).
    Define build_ptable : string list -> int -> ptable that builds a table for a given prefix length, using the two previous functions.
    Define walk_ptable : ptable -> string list that generates a sentence from a given ptable. Unless you put specific annotations, next_in_htable should be polymorphic enough to work on the field table of a ptable, so you don't have to rewrite one. If you want, since we now have proper sentence splitting, you can generate multi-sentence texts, by choosing randomly to continue from the start after encountering a "STOP".

Finally, the most funny texts are generated when mixing various kinds of inputs together (pirate stories, history books, recipes, political news, etc.).

    Define merge_ptables: ptable list -> ptable that combines several tables together (you may fail with an exception if the prefix sizes are inconsistent).

Now you can try and generate some texts using larger inputs, such as short novels! The prelude provides a few samples, otherwise Project Gutemberg is a good source. You can use display_quote: string list -> unit to display the generated texts.

let sauce_ptable =
   merge_ptables
     (List.map
        (fun s -> build_ptable s 2)
        (sentences some_cookbook_sauce_chapter)) ;;
display_quote (walk_ptable sauce_ptable) ;;

The given prelude

type ltable = (string * string list) list
type distribution =
  { total : int ;
    amounts : (string * int) list }
type htable = (string, distribution) Hashtbl.t
type ptable =
  { prefix_length : int ;
    table : (string list, distribution) Hashtbl.t }

let simple_0 =
  "I am a man and my dog is a good dog and a good dog makes a good man"

let simple_1 =
  "a good dad is proud of his son and a good son is proud of his dad"

let simple_2 =
  "a good woman is proud of her daughter and a good daughter is proud of her mom"

let simple_3 =
  "there is a beer in a fridge in a kitchen in a house in a land where \
   there is a man who has a house where there is no beer in the kitchen"

let multi_1 =
  "A good dad is proud of his son. \
   A good son is proud of his dad."

let multi_2 =
  "A good woman is proud of her daughter. \
   A good daughter is proud of her mom."

let multi_3 =
  "In a land of myths, and a time of magic, \
   the destiny of a great kingdom rests \
   on the shoulders of a young man."

let grimms_travelling_musicians =
  "An honest farmer had once an ass that had been a faithful servant ..."

let grimms_cat_and_mouse_in_partnership =
  "A certain cat had made the acquaintance of a mouse, and ..."

let the_war_of_the_worlds_chapter_one =
  "No one would have believed in the last years ..."

let some_cookbook_sauce_chapter =
  "Wine Chaudeau: Into a lined saucepan put ½ bottle Rhine ..."

let history_of_ocaml =
  "“Caml” was originally an acronym for Categorical ..."

*)

(* -- Part A -------------------------------------------------------------- *)

let is_digit chr = let int_code = int_of_char chr in int_code >= 48 && int_code <= 57
;;

let is_letter chr = let int_code = int_of_char (Char.uppercase chr) in int_code >= 65 && int_code <= 90
;;

let words str =
  let rec words' str' word word_list =
    if String.length str' = 0 then
      word_list @ [(Buffer.sub word 0 ((Buffer.length word)))]
    else
    if is_digit str'.[0] || is_letter str'.[0]
    then (Buffer.add_char word str'.[0];
          words' (String.sub str' 1 ((String.length str') - 1)) word word_list
         )
    else
      words' (String.sub str' 1 ((String.length str') - 1)) (Buffer.create 0) (word_list @ [(Buffer.sub word 0 ((Buffer.length word)))])
  in words' str (Buffer.create 0) []
;;

let upsert_into_ltable ltable k_word v_word = 
  let rec aux ltable' result is_updated =
    match ltable' with
    | [] when is_updated = true -> result
    | [] -> result @ [(k_word, [v_word])]
    | h::t -> let (k, v) = h in if k=k_word then aux t (result @ [(k_word, (v @ [v_word]))]) true else aux t (result @ [h]) is_updated
  in aux ltable [] false
;;

let build_ltable words =
  let rec build_ltable' (words: string list) ltable =
    match words with
    | [] -> ltable
    | h::[] -> upsert_into_ltable ltable h "STOP"
    | h::t -> build_ltable' t (upsert_into_ltable ltable h (List.hd t))
  in if List.length words > 0 then build_ltable' words [("START", [List.hd words])] else build_ltable' words []
;;      

let rec lkp_in_ltable ltable word = 
  match ltable with
  | [] -> None
  | h::t -> let (k, v) = h in if k=word then Some (k, v) else lkp_in_ltable t word
;;

let next_in_ltable table word =
  match lkp_in_ltable table word with
  | None -> ""
  | Some (k, v) -> List.nth v (Random.int (List.length v))
;;

let walk_ltable table =
  let rec walk_ltable' table word result =
    match word with
    | "STOP" -> result
    | "START" -> walk_ltable' table (next_in_ltable table word) result
    | some_word -> walk_ltable' table (next_in_ltable table word) (result @ [word])
  in walk_ltable' table "START" []
;;

(* -- Part B -------------------------------------------------------------- *)

let compute_distribution l =
  if List.length l = 0 then {total = 0; amounts = []} else
    let rec aux l amounts =
      List.fold_left (fun res elt -> let (k, v) = List.hd(List.rev res) in if k=elt then List.rev(List.tl(List.rev res)) @ [(k, v+1)] else res @ [(elt, 1)]) [(List.hd l, 1)] (List.tl l)
    in {total = List.length l; amounts = aux (List.sort (fun s1 s2 -> String.compare s1 s2) l) []}
;;

let insert_into_hashtbl hashtbl k_word v_word =
  try
    Hashtbl.replace hashtbl k_word ((Hashtbl.find hashtbl k_word) @ [v_word]);
    hashtbl
  with Not_found -> Hashtbl.add hashtbl k_word [v_word];
    hashtbl
;;

let build_htable_with_dupes words =
  let rec build_htable_with_dupes' words hashtbl = 
    match words with
    | [] -> hashtbl
    | h::[] -> insert_into_hashtbl hashtbl h "STOP"
    | h::t -> build_htable_with_dupes' t (insert_into_hashtbl hashtbl h (List.hd t))
  in build_htable_with_dupes' words (insert_into_hashtbl (Hashtbl.create 0) "START" (if List.length words = 0 then "STOP" else List.hd words))
;;

let build_htable words =
  let htable = Hashtbl.create 0 in
  Hashtbl.iter (fun k v -> Hashtbl.add htable k (compute_distribution v)) (build_htable_with_dupes words);
  htable
;;

let calculate_intervals distribution =
  List.fold_left (fun res elt -> let (word, count) = elt and (word', first, last) = List.hd(List.rev res) in 
                   if last = 0. then [(word, last, last +. (float_of_int count)/.(float_of_int distribution.total))]
                   else res @ [(word, last, last +. (float_of_int count)/.(float_of_int distribution.total))]
                 ) [("", 0., 0.)] distribution.amounts
;;

let next_in_htable table word =
  let rnd = Random.float 1. and intervals = calculate_intervals (Hashtbl.find table word) in
  let (result, _, _) = List.find (fun elt -> let (word, first, last) = elt in rnd >= first && rnd <= last) intervals in result
;;

let walk_htable table =
  let rec walk_ltable' table word result =
    match word with
    | "STOP" -> result
    | "START" -> walk_ltable' table (next_in_htable table word) result
    | some_word -> walk_ltable' table (next_in_htable table word) (result @ [word])
  in walk_ltable' table "START" []
;;

(* -- Part C -------------------------------------------------------------- *)

let is_valid_word_char chr = 
  (Char.code chr >= 128 && Char.code chr <= 255)
  || is_digit chr
  || is_letter chr
;;

let is_word_separator chr = 
  let valid_chars = [';'; ','; ':'; '-'; '"'; '\''; '?'; '!'; '.'] in
  try (List.find (fun elt -> elt = chr) valid_chars); true with Not_found -> false 
;;

let is_sentence_separator chr =
  let sentence_separators = ['.'; '!'; '?'] in
  try List.find (fun etl -> etl=chr) sentence_separators; true with Not_found -> false
;;

let list_of_string str =
  let rec aux str lst idx =
    if idx >= String.length str then lst else aux str (lst @ [str.[idx]]) (idx + 1)
  in aux str [] 0
;;

let sentences str =
  let char_to_string chr = String.make 1 chr in
  let add_word_to_phrase phrase word = if word <> "" then phrase @ [word] else phrase in
  let add_phrase_to_list phrases phrase = if phrase = [] then phrases else phrases @ [phrase] in
  let str_as_list = list_of_string str in
  let rec aux strlist phrases phrase word =
    match strlist with
    | [] -> add_phrase_to_list phrases (add_word_to_phrase phrase word)
    | h::t when is_valid_word_char h -> aux t phrases phrase (word ^ (char_to_string h))
    | h::t when is_word_separator h -> 
        if not (is_sentence_separator h)
        then aux t phrases (add_word_to_phrase(add_word_to_phrase phrase word) (char_to_string h)) ""
        else aux t (phrases @ [add_word_to_phrase (add_word_to_phrase phrase word) (char_to_string h)]) [] ""
    | h::t when is_sentence_separator h -> aux t (phrases @ [add_word_to_phrase (add_word_to_phrase phrase word) (char_to_string h)]) [] ""
    | h::t -> aux t phrases (add_word_to_phrase phrase word) ""
  in aux str_as_list [] [] ""
;;

let start pl =
  let rec aux res n = 
    if n = pl then res else aux (res @ ["START"]) (n+1)
  in aux [] 0
;;

let shift l x =
  if l = [] then [x] else (List.tl l) @ [x]
;;

let insert_into_amounts amounts word count =
  let rec aux amounts' result is_updated =
    match amounts' with
    | [] -> if is_updated then result else result @ [(word, count)]
    | h::t -> let (k,v)=h in if k=word then aux t (result @ [(k, (v+count))]) true else aux t (result @ [h]) is_updated
  in aux amounts [] false
;;

let insert_into_dist_ptable ptable k_list v_word count =
  try
    let found = Hashtbl.find ptable.table k_list in
    let updated = {total=found.total + count; amounts = (insert_into_amounts found.amounts v_word count)} in
    Hashtbl.replace ptable.table k_list updated;
    ptable
  with Not_found -> Hashtbl.add ptable.table k_list {total = count; amounts = [(v_word, count)]};
    ptable
;;

let build_ptable words pl =
  let rec aux words' ptable key_words =
    match words', key_words with
    | [], _ -> insert_into_dist_ptable ptable key_words "STOP" 1
    | h::t, _ when List.length key_words < pl -> aux t ptable (key_words @ [h])
    | h::t, _ -> aux t (insert_into_dist_ptable ptable key_words h 1) (shift key_words h)
  in aux ((start pl) @ words) {prefix_length=pl; table=(Hashtbl.create 0)} []
;;

let walk_ptable ptable =
  let rec walk_ptable' ptable lookup word result =
    match word with
    | "STOP" -> result
    | some_word -> walk_ptable' ptable (shift lookup word) (next_in_htable ptable.table (shift lookup word)) (result @ [word])
  in walk_ptable' ptable (start ptable.prefix_length) (next_in_htable ptable.table (start ptable.prefix_length)) []
;;

exception Incorrect_prefix_length;;

let merge_ptables tl =
  if List.length tl = 0 then raise Incorrect_prefix_length else
    let rec aux ptables result_ptable = 
      match ptables with
      | [] -> result_ptable
      | ptable::t -> (
          if result_ptable.prefix_length <> ptable.prefix_length then raise Incorrect_prefix_length
          else
            Hashtbl.iter (
              fun k_word_list distribution -> List.iter (fun amount -> let (word, count) = amount in insert_into_dist_ptable result_ptable k_word_list word count; ()) distribution.amounts
            ) ptable.table;
          aux t result_ptable
        )
    in aux (List.tl tl) (List.hd tl)
;;
