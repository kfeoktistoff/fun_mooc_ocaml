(*

Searching for strings in arrays (30/30 points)

    Write a function is_sorted : string array -> bool which checks if the values of the input array are sorted in strictly increasing order, implying that its elements are unique (use String.compare).
    Using the binary search algorithm, an element can be found very quickly in a sorted array.
    Write a function find : string array -> string -> int such that find arr word is the index of the word in the sorted array arr if it occurs in arr or -1 if word does not occur in arr.
    The number or array accesses will be counted, to check that you obtain the expected algorithmic complexity. Beware that you really perform the minimal number of accesses. For instance, if your function has to test the contents of a cell twice, be sure to put the result of the access in a variable, and then perform the tests on that variable.

*)

let is_sorted a = let rec is_sorted_comp ar ix = 
                    if Array.length ar <= 1 || ((Array.length ar) - 1) = ix 
                    then true 
                    else 
                    if (String.compare ar.(ix) ar.(ix+1)) = -1 
                    then is_sorted_comp ar (ix+1) 
                    else false 
  in is_sorted_comp a 0;;

let find arr word =
  let rec find' arr word f l =
    let m = (f + l) / 2 in
    if l < f then -1 else
      let cmp = (String.compare arr.(m)  word) in
      if (cmp = 0) then m else 
      if (cmp = -1) then (find' arr word (m+1) l) else find' arr word f (m-1) in 
  find' arr word 0 ((Array.length arr) - 1) ;;