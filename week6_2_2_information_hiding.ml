(*

Multiset (25/25 points)

A multiset is like a set, with the difference that a value can appear more than once.

    Implement a module MultiSet that implements the signature MultiSet_S.
    Define a function letters: string -> char MultiSet.t (where MultiSet is the module defined in the previous question). This function produces a multiset in which all characters are associated to the times they appear in the input string.
    Define a function anagram: string -> string -> bool that uses the previous function to tell if two words have the same multiset of characters.

The given prelude

module type MultiSet_S = sig

  (* A multi-set of type ['a t] is a collection of values of
     type ['a] that may occur several times. *)
  type 'a t

  (* [occurrences s x] return the number of time [x] occurs
     in [s]. *)
  val occurrences : 'a t -> 'a -> int

  (* The empty set has no element. There is only one unique
     representation of the empty set. *)
  val empty : 'a t

  (* [insert s x] returns a new multi-set that contains all
     elements of [s] and a new occurrence of [x]. Typically,
     [occurrences s x = occurrences (insert s x) x + 1]. *)
  val insert : 'a t -> 'a -> 'a t

  (* [remove s x] returns a new multi-set that contains all elements
     of [s] minus an occurrence of [x] (if [x] actually occurs in
     [s]). Typically, [occurrences s x = occurrences (remove s x) x -
     1] if [occurrences s x > 0]. *)
  val remove : 'a t -> 'a -> 'a t

end

*)

module MultiSet : MultiSet_S = struct
  type 'a t = 'a list

  let empty = []

  let occurrences s x =
    let rec occurrences' s' x n_occurrence =
      match s' with
      | empty -> n_occurrence
      | h::t when h=x -> occurrences' t x (n_occurrence + 1)
      | h::t -> occurrences' t x n_occurrence
    in occurrences' s x 0

  let insert s x = s @ [x]

  let remove s x =
    let rec remove' s' x res =
      match s' with
      | empty -> res
      | h::t when h=x -> remove' t x res
      | h::t -> remove' t x (res @ [h])
    in remove' s x empty

end;;

let letters word = 
  let rec letters' w' s =
    if String.length w' = 0 then s
    else letters' (String.sub w' 1 ((String.length w') - 1)) (MultiSet.insert s (String.get w' 0))
  in letters' word MultiSet.empty
;;

let anagram word1 word2 =
  if String.length word1 <> String.length word2 then false
  else
    let t1 = letters word1 and t2 = letters word2 in
    let rec anagram' letters1 multiset1 multiset2 =
      if String.length letters1 = 0 then true
      else 
      if (MultiSet.occurrences multiset2 (String.get letters1 0)) = (MultiSet.occurrences multiset1 (String.get letters1 0))
      then anagram' (String.sub letters1 1 ((String.length letters1) - 1)) multiset1 multiset2
      else false
    in anagram' word1 t1 t2 
;;
