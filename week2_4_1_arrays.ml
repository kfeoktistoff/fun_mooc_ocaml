(*

Finding the minimum (20/20 points)

Consider a non empty array of integers a.

    Write a function min : int array -> int that returns the minimal element of a.
    Write a function min_index : int array -> int that returns the index of the minimal element of a.

Do you think these functions work well on large arrays ?

    Define a variable it_scales and set it to "yes" or "no".

*)

let min a = 
  let min2 x y = if x < y then x else y in 
  let rec minarr ar ix mix = 
    if ((Array.length ar)) = ix 
    then ar.(mix) 
    else 
    if (min2 ar.(ix) ar.(mix)) = ar.(ix) 
    then minarr ar (ix+1) ix
    else minarr ar (ix+1) mix in minarr a 0 0;;

let min_index a = let rec min_index_comp ar ix = if (Array.length ar) = ix then ix else if (min ar) = ar.(ix) then ix else min_index_comp ar (ix+1) in min_index_comp a 0;;

let it_scales =
  "no" ;;
