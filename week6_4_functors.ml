(*

Char indexed hashtables (30/40 points)

Have a look at the documentation of module Hashtbl.

    Implement a module CharHashedType, compatible with the HashedType signature, where type t = char.
    Use the module defined in the previous question to instantiate the Hashtbl.Make functor as a module CharHashtbl.
    Reimplement the data structure of trie from a previous exercise, so that a hash table is used to represent the association between characters and children. To do so, complete the definition of module Trie, so that it is compatible with the given signature GenericTrie, whose 'a table type is instanciated to char indexed hash tables.
    Be careful, a hash table is not a purely functional data structure. Therefore, it must be copied when necessary!
    Note: you must neither change the signature nor the types of module Trie or the tests will fail.

The given prelude

module type GenericTrie = sig
  type 'a char_table
  type 'a trie = Trie of 'a option * 'a trie char_table
  val empty : unit -> 'a trie
  val insert : 'a trie -> string -> 'a -> 'a trie
  val lookup : 'a trie -> string -> 'a option
end

*)


module CharHashedType : Hashtbl.HashedType with type t = char = struct
  type t = char
  let equal char1 char2 = char1 = char2
  let hash some_char = int_of_char some_char
end;;

module CharHashtbl = Hashtbl.Make (CharHashedType);;

module Trie : GenericTrie with type 'a char_table = 'a CharHashtbl.t = struct
  type 'a char_table = 'a CharHashtbl.t
  type 'a trie = Trie of 'a option * 'a trie char_table

  let empty () = Trie (None, CharHashtbl.create 1);;

  let lookup trie w =
    let len = String.length w in
    let rec lookup' trie' idx =
      let Trie(value, children) = trie' in
      if idx >= len then value
      else
        let char = w.[idx] in
        let child = CharHashtbl.find children char
        in lookup' child (idx + 1)
    in
    lookup' trie 0
  ;;

  let insert trie w v =
    let len = String.length w in
    let rec aux trie idx =
      let Trie(value, children) = trie in
      if idx >= len
      then Trie(Some v, children)
      else
        let char = w.[idx] in
        let child = try
            CharHashtbl.find children char
          with Not_found ->
            let child = empty () in
            CharHashtbl.add children char child;
            child
        in
        aux child (idx + 1)
    in aux trie 0
  ;;

end